#include <stdio.h>

int cal_income(int price);

int cal_expense(int price);

int cal_profit(int price);

int cal_attendees(int price);

void display();

int cal_attendees(int price){
    const int att = 120;    
    return att - ((price-15) /5*20);    
}

int cal_income(int price){
    return price * cal_attendees(price);     
}

int cal_expense(int price){
    const int x = 500;
    return x + 3 * cal_attendees(price); 
}

int cal_profit(int price){
   return cal_income(price)-cal_expense(price);
}

void display(){
	int i=5,profit;
	for (i=5;i<=50;i+=5){
		 profit= cal_profit(i);
		 printf("Rs.%d \t Rs.%d\n",i,profit);
	}
}

int main()
{
   	
	printf("Here it shows the profits with relevance to the ticket prices.\n");
	printf("Price \t Profit\n");
	display();
	printf("As shown in the table, we can conclude that the maximum profit of Rs 1260 could be gained by setting Rs 25 as the ticket price. \n");
	
    return 0;
}
